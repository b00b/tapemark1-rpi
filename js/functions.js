/*
 * TODO:
 * - convert array prototypes to functions
 * - transform code in OO style
 * - format the output balanced
 * - textanimation?
 *
 * Facts:
 * Bei Ziehung von 10 aus 15, Reihenfolge ist wichtig:
 * 10897286400 Möglichkeiten
 *
 * Formel: (n über k) * k! = n! / (n-k)!
 */



$(document).ready(function () {

    var m = new MersenneTwister();
    var counter = 0;

    var width = $('.content').width();
    var height = $('.content').height();

    $('.content').width(height);
    $('.content').height(width);

    var strophes = {
        1: {
            1: {line: "der blendende / Ball / aus Feuer", goto:[23,24,25,31,32,33,34]},
            2: {line: "breitet sich / schnell aus", goto:[21,22,23,24,25,32,34]},
            3: {line: "dreißigmal / leuchtender / als die Sonne", goto:[21,22,23,24,25,31,32,33,34]},
            4: {line: "als er die Stratosphäre / erreicht", goto:[21,22,23,31,32,33,34]},
            5: {line: "die Spitze / der Wolke", goto:[23,24,25,31,32,33,34]},
            6: {line: "nimmt / die wohlbekannte Form / eines Pilzes an", goto:[21,22,23,24,25,32,34]},
        },
        2: {
            1: {line: "den Kopf / auf die Schulter / gedrückt", goto:[11,12,13,14,16,31,32,33,34]},
            2: {line: "die Haare / zwischen den Lippen", goto:[11,12,13,14,16,31,32,33,34]},
            3: {line: "lagen / regungslos / ohne zu sprechen", goto:[12,13,14,15,16,31,32,33,34]},
            4: {line: "bis er / die Finger / langsam bewegte", goto:[11,12,13,14,15,31,32,33,34]},
            5: {line: "festzuhalten / versuchend", goto:[11,12,13,15,16,31,32,33,34]},
        },
        3: {
            1: {line: "während die Vielheit / der Dinge / geschieht", goto:[11,12,13,15,16,21,22,23]},
            2: {line: "betrachte ich / ihre Rückkehr", goto:[11,13,14,15,16,21,22,23,24,25]},
            3: {line: "obwohl / die Dinge / blühen", goto:[12,13,14,15,16,23,24,25]},
            4: {line: "kehren sie / alle zurück / zu ihren Wurzeln", goto:[11,12,14,15,16,21,22,24,25]}
        }
    }


    var cnt = 0;

    renderPoem();

    setInterval(function() {
        renderPoem();
    }, 20000);


    function renderPoem() {

        $('.content').empty();
        var poem = makePoem(6)
        poem = renderLine(poem);
    }


    function renderLine(poem) {

        if(poem.length > 0) {

            var line = poem.splice(0, 1) + "<br>";
            $('.content').append(line).animate(
                {
                    opacity: "50"
                }, 100, function() {
                    renderLine(poem);
                }
            );

        } else {
            return false;
        }
    }


    function makePoem(blocks) {

        var poem = [];
        var i = 0;

        while(i  < blocks)
        {
            var block = makeBlock();

            block = formatBlock(block);
            block.forEach(function (e) {
                poem.push(e)
            });
            i++;
        }

        return poem;
    }


    function makeBlock() {

        var count = 0;

        while(count != 24) {

            var path = [];
            var remaining = [11, 12, 13, 14, 15, 16, 21, 22, 23, 24, 25, 31, 32, 33, 34];
            var index = randSplice(remaining);
            var poem = getlineSegments(index);
            path.push(index);

            for (i = 0; i < 9; i++) {

                var merged = booleanAnd(remaining, getNextIndex(index));
                index = randomElement(merged);
                if(index == undefined) {
                    break;
                }
                path.push(index);
                remaining.splice(remaining.indexOf(index),1);
                //console.log('merged: ' + merged + ' remain: ' + remaining + ' getIndex: ' +  getNextIndex(index) + ' index: ' + index);
                poem = poem.concat(getlineSegments(index))
            }
            count = poem.length;
        }

        return poem;
    }


    function formatBlock(poem) {
        var formatedPoem = [];

        while(poem.length > 0) {
            var line = [];
            for(i = 0; i < 4; i++) {
                if (poem[i] != undefined) {
                    line.push(poem[i].trim());
                }
            }
            poem.splice(0, 4);
            line = line.join(" ");
            line = capitalizeFirstLetter(line);
            formatedPoem.push(line);
        }
        formatedPoem.push("<p>");
        return formatedPoem;
    }


    function getlineSegments(index) {

        var block = num2digits(index)[0];
        var line = num2digits(index)[1];
        return strophes[block][line].line.split("/");
    }

    function num2digits(num) {
        if(num == undefined) {
            console.log('undefined')
            return 0;
        } else {
            var digits = num.toString().split('');
            var realDigits = digits.map(Number);
            return realDigits;
        }
    }

    function booleanAnd(array1, array2) {
        var combined = [];
        array1.forEach(function(el) {
            if (array2.indexOf(el) > -1) {
                combined.push(el);
            }
        })
        return combined;
    }

    function getNextIndex(index) {
        var block = num2digits(index)[0];
        var line = num2digits(index)[1];
        return strophes[block][line].goto;
    }

    function randSplice(array) {
        var ri = Math.floor(m.random() * array.length);
        var rs = array.splice(ri, 1);
        return rs[0];
    }

    function randomElement(array) {
        return array[Math.floor(m.random() * array.length)]
    }

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
});
