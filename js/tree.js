$(document).ready(function () {

    var m = new MersenneTwister();
    var counter = 0;

    var adjazenz = [
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1],
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1],
        [1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1],
        [1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1],
        [0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1],
        [1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1],
        [1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
        [1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0],
        [0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0],
        [1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0]
    ];

    var strophes = {
        1: {
            1: {line: "der blendende / Ball / aus Feuer", goto: [23, 24, 25, 31, 32, 33, 34]},
            2: {line: "expandiert / schnell", goto: [21, 22, 23, 24, 25, 32, 34]},
            3: {line: "dreißigmal / leuchtender / als die Sonne", goto: [21, 22, 23, 24, 25, 31, 32, 33, 34]},
            4: {line: "erreicht er / die Stratosphäre", goto: [21, 22, 23, 31, 32, 33, 34]},
            5: {line: "die Spitze / der Wolke", goto: [23, 24, 25, 31, 32, 33, 34]},
            6: {line: "bekommt / die wohl bekannte Form / eines Pilzes", goto: [21, 22, 23, 24, 25, 32, 34]},
        },
        2: {
            1: {line: "den Kopf / an die Schulter / gepresst", goto: [11, 12, 13, 14, 16, 31, 32, 33, 34]},
            2: {line: "die Haare / zwischen den Lippen", goto: [11, 12, 13, 14, 16, 31, 32, 33, 34]},
            3: {line: "lagen / regungslos / ohne zu sprechen", goto: [12, 13, 14, 15, 16, 31, 32, 33, 34]},
            4: {line: "bis er / seine Finger / langsam bewegte", goto: [11, 12, 13, 14, 15, 31, 32, 33, 34]},
            5: {line: "festzuhalten / versuchend", goto: [11, 12, 13, 15, 16, 31, 32, 33, 34]},
        },
        3: {
            1: {line: "während die Vielheit / der Dinge / geschieht", goto: [11, 12, 13, 15, 16, 21, 22, 23]},
            2: {line: "betrachte ich / ihre Rückkehr", goto: [11, 13, 14, 15, 16, 21, 22, 23, 24, 25]},
            3: {line: "obwohl / die Dinge / erblühen", goto: [12, 13, 14, 15, 16, 23, 24, 25]},
            4: {line: "kehren sie / alle zurück / an ihre Wurzel", goto: [11, 12, 14, 15, 16, 21, 22, 24, 25]}
        }
    }


    /**
     *
     */
    var start = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14];
    var workingcopy = adjazenz;
    var tree = {};

    makeLeave(workingcopy, 0)



    function Node(node, parents, value) {
        this.node = node;
        this.parents = parents;
        this.value = value;
    }


    function makeTree(matrix, start) {

        var tree = [];

        while(cnt < 10 && cnt >= 0) {

            for (i = 0; i < 15; i++) {
                //if(matrix[][i])
            }
        }


        return tree;
    }

    function makeBranch(matrix, node) {

        for (i = 0; i < 15; i++) {
            if (workingcopy[node][i] == 1) {
                matrix = setZero(matrix, i);
                break;
            }
        }
        console.log(matrix);
    }


    function setZero(matrix, index) {
        matrix[index].forEach(function(el, col) {
            matrix[index][col] = 0;
            matrix[col][index] = 0;
        })
        return(matrix);
    }


    function makeTree(matrix, row, next) {

        var index, step;
        var row = matrix[next];

        while(step == true && index < 15 && index >= 0) {
        }
    }


    function forwardStep(obj) {
        var i = obj.i;
        var j = obj.j;
        var matrix = obj.matrix;
        var next = 1;
    }

    function randomElement(array) {
        return array[Math.floor(m.random() * array.length)]
    }

    function booleanAnd(array1, array2) {
        var combined = [];
        array1.forEach(function(el) {
            if (array2.indexOf(el) > -1) {
                combined.push(el);
            }
        })
        return combined;
    }

    function booleanNot(array1, array2) {
        var combined = [];
        array1.forEach(function(el) {
            if (array2.indexOf(el) > -1) {
                combined.push(el);
            }
        })
        return combined;
    }
});